#include <cmath>
#include <ros/ros.h>
#include <geometry_msgs/Pose2D.h>
#include <tf/transform_listener.h>

int main(int argc,char **argv){
  ros::init(argc, argv, "carto2pose");
  ros::NodeHandle node;
  ros::Rate rate(1000);
  
  tf::TransformListener listener;
  geometry_msgs::PoseStamped target_pose;

  ros::Publisher carto2pose_pub = node.advertise<geometry_msgs::Pose2D>("/base_link",100);
  geometry_msgs::Pose2D mr1_pose;
  double row, pitch, yaw;
  const static float LIDAR_POSITION = 0.33564;//lidarとMR1の中心の距離[m]
  
  ros::Time old_transform_stamp;
  float tmp_x, tmp_y;//地図上のMR1の中心
  int mr1_pose_x_tmp, mr1_pose_y_tmp, mr1_theta_tmp;

  while(ros::ok()){
    tf::StampedTransform transform;
    
    try{
      ROS_INFO("wait");
      listener.waitForTransform("/map", "/horizontal_laser_link", ros::Time(0), ros::Duration(1)); 
      listener.lookupTransform("/map", "/horizontal_laser_link", ros::Time(0), transform);
      //listener.lookupTransform("/map", "/horizontal_laser_link", ros::Time(0), transform);
      ROS_INFO("GET");
      
      if(old_transform_stamp != transform.stamp_){
	
	tf::Matrix3x3 matrix(transform.getRotation());
	matrix.getRPY(row, pitch, yaw);
	//mr1_pose.theta = -yaw;//lidarがmr1の後ろについているため	

	//0.4と0.5はシュミレータの値
        tmp_x= transform.getOrigin().x() - 0.4 - LIDAR_POSITION*cos(-yaw);
	tmp_y = transform.getOrigin().y() + 0.50 + LIDAR_POSITION*sin(-yaw);

	
	//0.1はシュミレータのbagデータで検証して誤差が均等になるよう設定した。
	//mr1_pose_x_tmp = (int)((-tmp_y + 0.1)*100);
	//mr1_pose_y_tmp = (int)((-tmp_x - 0.21)*100);
	//mr1_theta_tmp = (int)(-yaw*100);

	
	//桁を落とす
	mr1_pose.x = (-tmp_y + 0.1);// - std::fmod((-tmp_y + 0.1), 0.01);
	mr1_pose.y = (-tmp_x - 0.21) + 0.02;// - std::fmod((-tmp_x - 0.21), 0.01);
	mr1_pose.theta = -yaw;// - std::fmod((-yaw), 0.001);
	
	carto2pose_pub.publish(mr1_pose);
	old_transform_stamp = transform.stamp_;
	ROS_INFO("send");

      }

    }catch(tf::TransformException ex){
      ROS_ERROR("%s", ex.what());
      ros::Duration(0.1).sleep();
      continue;
    }   
    rate.sleep();
    
  }
  return 0;
}
